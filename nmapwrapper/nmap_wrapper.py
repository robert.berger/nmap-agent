from tcpserver.server import prepare_agent_for_remote_requests

from nmapwrapper.nmap_actions import NMAPActions
        
from . import logger

class NMapWrapper(object):
    # ##
    # Save New Request for Handling
    def __init__(self, request):
        logger.debug("Inside NMapWrapper ... Ready to Handle New Request \"%s\" !", request)
        self.request = request
    
    # ##
    # Parse string and find {request-type, net-object, required-action} ... ex nmap:host:discovery
    def to_parse( self ):
        # Check requests beginning 
        if not self.request.startswith("nmap:"):
            raise Exception("Unknown Request Type ... Can Handle Only nmap: !")

        # Check request target & action
        t = self.request.split(":")
        if len(t) > 5:
            raise Exception("Uknown Request Format ... Can handle Only nmap:host:discovery")

        self.tool   = t[0]
        self.target = t[1]
        self.action = t[2]

        if len(t) > 3:
            self.server = t[3]

        # ends with !
        if self.action.endswith("!"):
            # Remove exclamation mark
            self.action = self.action[:-1]

        # if self.key.endswith("port:scan!"):
        #    logger.debug("Have a PORT-SCAN request for NMAP")
        #    self.nmap_open_ports()        
    
    # ##
    # Call right tool for requested target & action
    def call_right_tool(self):
        if self.tool.lower() != "nmap":
            raise Exception("Call Right Tool Error ... ABORTING !")

        # This Agent is only responsible for NMAP network-tool
        #logger.debug("++++++ target = %s , %s, %s", self.target, self.action, self.server)
        
        if self.target.lower() == "host":
            return NMAPActions( target = self.target, action = self.action )
        elif self.target.lower() == "port":
            return NMAPActions( target = self.target, action = self.action, server = self.server )
        
    # ##
    # Handle New Request    
    def handle_new_request(self):
        # Step 1 --> Parse the long-string of request from client to server 
        self.to_parse()

        # Step 2 --> Prepare appropriate tool for target & action
        tool = self.call_right_tool()

        # Step 3 --> Call selected tool to execute requested action on specified target object
        targets_found_str = tool.call_nmap_target_action()

        #logger.debug("Targets Found by NMAP ... %s", targets_found_str)

        return targets_found_str
 
########
#### 
def main():
    #logger.debug("NMap-Wrapper Ready to Start !")    
    prepare_agent_for_remote_requests()
